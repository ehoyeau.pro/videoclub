<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="videoClub.css">
    <title>index</title>
</head>
<body>

<?php
$dbh = new PDO($dsn="mysql:host=localhost;dbname=videoClub;port=3306;charset=utf8",
    $username="eleve",
    $password="bonjour");

//test la connexion et renvoie l'erreur si non possible de se connecter
try {
    $dbh =new PDO($dsn, $username, $password);
} catch (PDOException $e) {
    var_dump($e);
}

//var_dump($dbh);


$listefilms = $dbh->prepare("SELECT * FROM films");
$listefilms->execute();

echo '<h1>';
echo 'LISTE DES FILMS';
echo'</h1>';


echo '<th><b>Liste des films</b></th>';

while($lfilms = $listefilms->fetch()){
    echo'<ul class="corpstab">';
    echo '<li>' . $lfilms['titre'] . '</li>';
    echo'</ul>';
}

?>

<form action="">
    <input type="submit" value="Ajout d'un film">
</form>

<div id="ajoutFilm">

    <form>
        <label for="realisateur"> Réalisateur : </label>
        <select>
            <?PHP
            $listereal = $dbh->prepare("SELECT * FROM realisateur");
            $listereal->execute();

            while($lreal = $listereal->fetch()){ ?>
            <option value="<?PHP
                $real = $lreal['id'];
                $real .= ' '.$lreal['nom'];
                echo $real;?>"><?PHP echo $real?></option><?PHP
            }?>
        </select>

    <br><br>
        <label for="realisateur"> Genre : </label>
        <select>
            <?PHP
            $listegenres = $dbh->prepare("SELECT * FROM genres");
            $listegenres->execute();

            while($lgenres = $listegenres->fetch()){ ?>
                <option value="<?PHP
                $genres = $lgenres['id'];
                $genres .= ' '.$lgenres['nom'];
                echo $genres;?>"><?PHP echo $genres?></option><?PHP
            }?>
        </select><br><br>
        <label for="titre"> Titre : </label>
        <input type="text" id="titre" name="titre"><br><br>
        <label for="anreal"> Année de réalisation : </label>
        <input type="text" id="anreal" name="anreal"><br><br>
    </form>

</div>

</body>
</html>
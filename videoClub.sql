DROP DATABASE IF EXISTS videoClub;
CREATE DATABASE videoClub;
USE videoClub;

CREATE TABLE IF NOT EXISTS clients
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(255) NOT NULL,
    prenom VARCHAR(255) NOT NULL,
    telephone_fixe VARCHAR(255) NOT NULL,
    adresse VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS location
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    vhs_id INT(11) NOT NULL,
    client_id INT(11) NOT NULL,
    debut DATETIME NOT NULL,
    fin DATETIME NOT NULL
);

CREATE TABLE IF NOT EXISTS vhs
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    film_id INT(11) NOT NULL,
    etat_id INT(11) NOT NULL
);

INSERT INTO vhs (film_id, etat_id)
VALUES
('1','2'),
('1','1'),
('3','4'),
('3','4'),
('3','4'),
('3','3'),
('2','2'),
('2','1'),
('2','4'),
('4','2'),
('4','4'),
('4','1'),
('5','2'),
('5','4'),
('6','1'),
('6','1'),
('6','3'),
('6','2'),
('6','2')
;

CREATE TABLE IF NOT EXISTS films
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    titre VARCHAR(255) NOT NULL,
    realisateur_id INT(11) NOT NULL,
    genre_id INT(11) NOT NULL,
    annee VARCHAR(4) NOT NULL
);

INSERT INTO films (titre, realisateur_id, genre_id, annee)
VALUES
('Les_Killers', '1', '2', '1996'),
('Les_gendarmes_de_la_bourboule','2','1','2010'),
('T_es_pas_migon', '3','3','2001'),
('C_est_pas_la_joie', '4', '2', '1985'),
('Oiseau_de_malheur','5', '3', '1999'),
('Wouaip','5','4','1978')
;

CREATE TABLE IF NOT EXISTS genres
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(255) NOT NULL
);

INSERT INTO genres (nom)
VALUES
('Comedie'),
('Romantique'),
('Pestacle'),
('Erotique'),
('Horreur')
;

CREATE TABLE IF NOT EXISTS etat
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    nom TINYINT(1) NOT NULL
);

INSERT INTO etat (nom)
VALUES
('1'),
('2'),
('3'),
('4')
;

CREATE TABLE IF NOT EXISTS realisateur
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(255) NOT NULL
);

INSERT INTO realisateur (nom)
VALUES
('Bertier'),
('James'),
('Chaotsecou'),
('Dupont'),
('Le_Heron')
;